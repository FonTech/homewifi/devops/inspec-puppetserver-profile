describe package('git') do
  it { should be_installed }
end

describe package('puppetserver') do
  it { should be_installed }
  its('version') { should eq attribute('puppet_server_version') }
end

describe service('puppetserver') do
  it { should be_running }
end

describe file('/etc/puppetlabs/puppet/autosign.conf') do
  it { should exist }
end
